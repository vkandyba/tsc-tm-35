package ru.vkandyba.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.model.Task;
import ru.vkandyba.tm.repository.TaskRepository;

import java.util.List;

public class TaskServiceTest {

    @NotNull
    TaskRepository taskRepository = new TaskRepository();

    @NotNull
    TaskService taskService = new TaskService(taskRepository);

    @NotNull
    String taskId = "123456790";

    @NotNull
    String taskName = "test";

    @NotNull
    Integer taskIndex = 0;

    @NotNull
    String userId = "1";

    @NotNull
    Status status = Status.COMPLETED;

    @Before
    public void before(){
        Task task = new Task();
        task.setId(taskId);
        task.setUserId(userId);
        task.setName(taskName);
        taskService.add(task);
    }

    @Test
    public void findAll() {
        List<Task> tasks = taskService.findAll(userId);
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    public void findByName() {
        @Nullable final Task task = taskService.findByName(userId, taskName);
        Assert.assertNotNull(task);
    }

    @Test
    public void removeByName() {
        @Nullable final Task task = taskService.removeByName(userId, taskName);
        Assert.assertNotNull(task);
    }

    @Test
    public void startById() {
        @Nullable final Task task = taskService.startById(userId, taskId);
        Assert.assertNotNull(task);
    }

    @Test
    public void startByIndex() {
        @Nullable final Task task = taskService.startByIndex(userId, taskIndex);
        Assert.assertNotNull(task);
    }

    @Test
    public void startByName() {
        @Nullable final Task task = taskService.startByName(userId, taskName);
        Assert.assertNotNull(task);
    }

    @Test
    public void finishById() {
        @Nullable final Task task = taskService.finishById(userId, taskId);
        Assert.assertNotNull(task);
    }

    @Test
    public void finishByIndex() {
        @Nullable final Task task = taskService.finishByIndex(userId, taskIndex);
        Assert.assertNotNull(task);
    }

    @Test
    public void finishByName() {
        @Nullable final Task task = taskService.finishByName(userId, taskName);
        Assert.assertNotNull(task);
    }

    @Test
    public void changeStatusById() {
        @Nullable final Task task = taskService.changeStatusById(userId, taskId, status);
        Assert.assertNotNull(task);
    }

    @Test
    public void changeStatusByIndex() {
        @Nullable final Task task = taskService.changeStatusByIndex(userId, taskIndex, status);
        Assert.assertNotNull(task);
    }

    @Test
    public void changeStatusByName() {
        @Nullable final Task task = taskService.changeStatusByName(userId, taskName, status);
        Assert.assertNotNull(task);
    }

    @Test
    public void clear() {
        taskService.clear(userId);
        List<Task> task = taskService.findAll(userId);
        Assert.assertEquals(0, task.size());
    }

    @Test
    public void existsByIndex() {
        @Nullable final Boolean task = taskService.existsByIndex(userId, taskIndex);
        Assert.assertTrue(task);
    }

    @Test
    public void existsById() {
        @Nullable final Boolean task = taskService.existsById(userId, taskId);
        Assert.assertTrue(task);
    }

    @Test
    public void findById() {
        @Nullable final Task task = taskService.findById(userId, taskId);
        Assert.assertNotNull(task);
    }

    @Test
    public void findByIndex() {
        @Nullable final Task task = taskService.findByIndex(userId, taskIndex);
        Assert.assertNotNull(task);
    }

    @Test
    public void removeById() {
        @Nullable final Task task = taskService.removeById(userId, taskId);
        Assert.assertNotNull(task);
    }

    @Test
    public void removeByIndex() {
        @Nullable final Task task = taskService.removeByIndex(userId, taskIndex);
        Assert.assertNotNull(task);
    }

}
