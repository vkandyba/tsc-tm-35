package ru.vkandyba.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.model.Task;
import java.util.List;

public class TaskRepositoryTest {

    @NotNull
    TaskRepository taskRepository = new TaskRepository();

    @NotNull
    String taskId = "123456790";

    @NotNull
    String taskIdIncorrect = "dd";

    @NotNull
    String taskName = "test";

    @NotNull
    String taskNameIncorrect = "dd";

    @NotNull
    Integer taskIndex = 0;

    @NotNull
    Integer taskIndexIncorrect = 2;

    @NotNull
    String userId = "1";

    @NotNull
    Status status = Status.COMPLETED;

    @Before
    public void before(){
        Task task = new Task();
        task.setId(taskId);
        task.setUserId(userId);
        task.setName(taskName);
        taskRepository.add(task);
    }

    @Test
    public void findAll() {
        List<Task> tasks = taskRepository.findAll(userId);
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    public void findByName() {
        @Nullable final Task task = taskRepository.findByName(userId, taskName);
        @Nullable final Task taskIncorrect = taskRepository.findByName(userId, taskNameIncorrect);
        Assert.assertNotNull(task);
        Assert.assertNull(taskIncorrect);
    }

    @Test
    public void removeByName() {
        @Nullable final Task task = taskRepository.removeByName(userId, taskName);
        @Nullable final Task taskIncorrect = taskRepository.removeByName(userId, taskNameIncorrect);
        Assert.assertNotNull(task);
        Assert.assertNull(taskIncorrect);
    }

    @Test
    public void startById() {
        @Nullable final Task task = taskRepository.startById(userId, taskId);
        @Nullable final Task taskIncorrect = taskRepository.startById(userId, taskIdIncorrect);
        Assert.assertNotNull(task);
        Assert.assertNull(taskIncorrect);
    }

    @Test
    public void startByIndex() {
        @Nullable final Task task = taskRepository.startByIndex(userId, taskIndex);
        @Nullable final Task taskIncorrect = taskRepository.startByIndex(userId, taskIndexIncorrect);
        Assert.assertNotNull(task);
        Assert.assertNull(taskIncorrect);
    }

    @Test
    public void startByName() {
        @Nullable final Task task = taskRepository.startByName(userId, taskName);
        @Nullable final Task taskIncorrect = taskRepository.startByName(userId, taskNameIncorrect);
        Assert.assertNotNull(task);
        Assert.assertNull(taskIncorrect);
    }

    @Test
    public void finishById() {
        @Nullable final Task task = taskRepository.finishById(userId, taskId);
        @Nullable final Task taskIncorrect = taskRepository.finishById(userId, taskIdIncorrect);
        Assert.assertNotNull(task);
        Assert.assertNull(taskIncorrect);
    }

    @Test
    public void finishByIndex() {
        @Nullable final Task task = taskRepository.finishByIndex(userId, taskIndex);
        @Nullable final Task taskIncorrect = taskRepository.finishByIndex(userId, taskIndexIncorrect);
        Assert.assertNotNull(task);
        Assert.assertNull(taskIncorrect);
    }

    @Test
    public void finishByName() {
        @Nullable final Task task = taskRepository.finishByName(userId, taskName);
        @Nullable final Task taskIncorrect = taskRepository.finishByName(userId, taskNameIncorrect);
        Assert.assertNotNull(task);
        Assert.assertNull(taskIncorrect);
    }

    @Test
    public void changeStatusById() {
        @Nullable final Task task = taskRepository.changeStatusById(userId, taskId, status);
        Assert.assertNotNull(task);
    }

    @Test
    public void changeStatusByIndex() {
        @Nullable final Task task = taskRepository.changeStatusByIndex(userId, taskIndex, status);
        @Nullable final Task taskIncorrect = taskRepository.changeStatusByIndex(userId, taskIndexIncorrect, status);
        Assert.assertNotNull(task);
        Assert.assertNull(taskIncorrect);
    }

    @Test
    public void changeStatusByName() {
        @Nullable final Task task = taskRepository.changeStatusByName(userId, taskName, status);
        @Nullable final Task taskIncorrect = taskRepository.changeStatusByName(userId, taskNameIncorrect, status);
        Assert.assertNotNull(task);
        Assert.assertNull(taskIncorrect);
    }

    @Test
    public void clear() {
        taskRepository.clear(userId);
        List<Task> task = taskRepository.findAll(userId);
        Assert.assertEquals(0, task.size());
    }

    @Test
    public void existsByIndex() {
        @Nullable final Boolean task = taskRepository.existsByIndex(userId, taskIndex);
        @Nullable final Boolean taskIncorrect = taskRepository.existsByIndex(userId, taskIndexIncorrect);
        Assert.assertTrue(task);
        Assert.assertFalse(taskIncorrect);
    }

    @Test
    public void existsById() {
        @Nullable final Boolean task = taskRepository.existsById(userId, taskId);
        @Nullable final Boolean taskIncorrect = taskRepository.existsById(userId, taskIdIncorrect);
        Assert.assertTrue(task);
        Assert.assertFalse(taskIncorrect);
    }

    @Test
    public void findById() {
        @Nullable final Task task = taskRepository.findById(userId, taskId);
        @Nullable final Task taskIncorrect = taskRepository.findById(userId, taskIdIncorrect);
        Assert.assertNotNull(task);
        Assert.assertNull(taskIncorrect);
    }

    @Test
    public void findByIndex() {
        @Nullable final Task task = taskRepository.findByIndex(userId, taskIndex);
        Assert.assertNotNull(task);
    }

    @Test
    public void removeById() {
        @Nullable final Task task = taskRepository.removeById(userId, taskId);
        @Nullable final Task taskIncorrect = taskRepository.removeById(userId, taskIdIncorrect);
        Assert.assertNotNull(task);
        Assert.assertNull(taskIncorrect);
    }

    @Test
    public void removeByIndex() {
        @Nullable final Task task = taskRepository.removeByIndex(userId, taskIndex);
        @Nullable final Task taskIncorrect = taskRepository.removeByIndex(userId, taskIndexIncorrect);
        Assert.assertNotNull(task);
        Assert.assertNull(taskIncorrect);
    }

}
