package ru.vkandyba.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.repository.IBusinessRepository;
import ru.vkandyba.tm.model.AbstractBusinessEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractBusinessRepository<E extends AbstractBusinessEntity>
        extends AbstractRepository<E> implements IBusinessRepository<E> {

    @NotNull
    @Override
    public Boolean existsByIndex(@NotNull String userId, @NotNull Integer index) {
        final Optional<E> entity = Optional.ofNullable(findByIndex(userId, index));
        return entity.isPresent();
    }

    @NotNull
    @Override
    public Boolean existsById(@NotNull String userId, @NotNull String id) {
        final Optional<E> entity = Optional.ofNullable(findById(userId, id));
        return entity.isPresent();
    }

    @Nullable
    @Override
    public E findById(@NotNull String userId, @NotNull String id) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .filter(e -> id.equals(e.getId()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public E findByIndex(@NotNull String userId, @NotNull Integer index) {
        final List<E> entityList = findAll(userId);
        if(index > entityList.size() - 1) return null;
        return entityList.get(index);
    }

    @Nullable
    @Override
    public E removeById(@NotNull String userId, @NotNull String id) {
        final Optional<E> entity = Optional.ofNullable(findById(userId, id));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

    @Nullable
    @Override
    public E removeByIndex(@NotNull String userId, @NotNull Integer index) {
        final Optional<E> entity = Optional.ofNullable(findByIndex(userId, index));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

    @NotNull
    @Override
    public E add(@NotNull String userId, @NotNull E entity) {
        entity.setUserId(userId);
        list.add(entity);
        return entity;
    }

    @Override
    public void remove(@NotNull String userId,@NotNull E entity) {
        list.remove(entity);
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull String userId) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull String userId, @NotNull Comparator<E> comparator) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

}
