package ru.vkandyba.tm.exception.system;

import ru.vkandyba.tm.constant.TerminalConst;
import ru.vkandyba.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException() {
        super("Error! Unknown Command Exception. Use '" + TerminalConst.HELP + "' for command list.");
    }
}
