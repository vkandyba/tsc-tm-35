package ru.vkandyba.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.endpoint.IProjectEndpoint;
import ru.vkandyba.tm.api.service.ServiceLocator;
import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.model.Project;
import ru.vkandyba.tm.model.Session;

import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Nullable
    @Override
    public List<Project> findAllProjects(@NotNull Session session) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAll(session.getUserId());
    }

    @Override
    public @Nullable Project addProject(@NotNull Session session, @NotNull Project project) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().add(session.getUserId(),project);
    }

    @Override
    public @Nullable Project changeProjectStatusById(@NotNull Session session, @NotNull String projectId, @NotNull Status status) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().changeStatusById(session.getUserId(), projectId, status);
    }

    @Override
    public @Nullable Project changeProjectStatusByIndex(@NotNull Session session, @NotNull Integer index, @NotNull Status status) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().changeStatusByIndex(session.getUserId(), index, status);
    }

    @Override
    public @Nullable Project changeProjectStatusByName(@NotNull Session session, @NotNull String name, @NotNull Status status) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().changeStatusByName(session.getUserId(), name, status);
    }

    @Override
    public @Nullable Project finishProjectById(@NotNull Session session, @NotNull String projectId) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().finishById(session.getUserId(), projectId);
    }

    @Override
    public @Nullable Project finishProjectByIndex(@NotNull Session session, @NotNull Integer index) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().finishByIndex(session.getUserId(), index);
    }

    @Override
    public @Nullable Project finishProjectByName(@NotNull Session session, @NotNull String name) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().finishByName(session.getUserId(), name);
    }

    @Override
    public @Nullable Project removeProjectById(@NotNull Session session, @NotNull String projectId) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().removeById(session.getUserId(), projectId);
    }

    @Override
    public @Nullable Project removeProjectByIndex(@NotNull Session session, @NotNull Integer index) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().removeByIndex(session.getUserId(), index);
    }

    @Override
    public @Nullable Project removeProjectByName(@NotNull Session session, @NotNull String name) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().removeByName(session.getUserId(), name);
    }

    @Override
    public @Nullable Project showProjectById(@NotNull Session session, @NotNull String projectId) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findById(session.getUserId(), projectId);
    }

    @Override
    public @Nullable Project showProjectByIndex(@NotNull Session session, @NotNull Integer index) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findByIndex(session.getUserId(), index);
    }

    @Override
    public @Nullable Project startProjectById(@NotNull Session session, @NotNull String projectId) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().startById(session.getUserId(), projectId);
    }

    @Override
    public @Nullable Project startProjectByIndex(@NotNull Session session, @NotNull Integer index) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().startByIndex(session.getUserId(), index);
    }

    @Override
    public @Nullable Project startProjectByName(@NotNull Session session, @NotNull String name) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().startByName(session.getUserId(), name);
    }

    @Override
    public @Nullable Project updateProjectByIndex(@NotNull Session session, @NotNull Integer index, @NotNull String name, @NotNull String description) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().updateByIndex(session.getUserId(), index, name, description);
    }

    @Override
    public @Nullable Project updateProjectById(@NotNull Session session, @NotNull String projectId, @NotNull String name, @NotNull String description) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().updateById(session.getUserId(), projectId, name, description);
    }

    @Override
    public @Nullable Project removeProjectWithTasksById(@NotNull Session session, @NotNull String projectId) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().removeById(session.getUserId(), projectId);
    }

    @Override
    public void clearProjects(@NotNull Session session) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().clear(session.getUserId());
    }

}
