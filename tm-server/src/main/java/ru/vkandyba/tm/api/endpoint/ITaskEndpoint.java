package ru.vkandyba.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.model.Task;
import ru.vkandyba.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {

    @Nullable
    @WebMethod
    List<Task> findAllTasks(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    );

    @Nullable
    @WebMethod
    Task addTask(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "task", partName = "task") @NotNull Task task
    );

    @Nullable
    @WebMethod
    Task changeTaskStatusById(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "taskId", partName = "taskId") @NotNull String taskId,
            @WebParam(name = "status", partName = "status") @NotNull Status status
    );

    @Nullable
    @WebMethod
    Task changeTaskStatusByIndex(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index,
            @WebParam(name = "status", partName = "status") @NotNull Status status
    );

    @Nullable
    @WebMethod
    Task changeTaskStatusByName(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "status", partName = "status") @NotNull Status status
    );

    @Nullable
    @WebMethod
    Task finishTaskById(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "taskId", partName = "taskId") @NotNull String taskId
    );

    @Nullable
    @WebMethod
    Task finishTaskByIndex(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index
    );

    @Nullable
    @WebMethod
    Task finishTaskByName(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "name", partName = "name") @NotNull String name
    );

    @Nullable
    @WebMethod
    Task removeTaskById(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "taskId", partName = "taskId") @NotNull String taskId
    );

    @Nullable
    @WebMethod
    Task removeTaskByIndex(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index
    );

    @Nullable
    @WebMethod
    Task removeTaskByName(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "name", partName = "name") @NotNull String name
    );

    @Nullable
    @WebMethod
    Task showTaskById(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "taskId", partName = "taskId") @NotNull String taskId
    );

    @Nullable
    @WebMethod
    Task showTaskByIndex(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index
    );

    @Nullable
    @WebMethod
    Task startTaskById(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "taskId", partName = "taskId") @NotNull String taskId
    );

    @Nullable
    @WebMethod
    Task startTaskByIndex(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index
    );

    @Nullable
    @WebMethod
    Task startTaskByName(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "name", partName = "name") @NotNull String name
    );

    @Nullable
    @WebMethod
    Task updateTaskByIndex(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "description", partName = "description") @NotNull String description
    );

    @Nullable
    @WebMethod
    Task updateTaskById(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "taskId", partName = "taskId") @NotNull String taskId,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "description", partName = "description") @NotNull String description

    );

    @Nullable
    @WebMethod
    Task removeTaskWithTasksById(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "taskId", partName = "taskId") @NotNull String taskId
    );

    @Nullable
    @WebMethod
    void clearTasks(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    );

    @Nullable
    @WebMethod
    Task unbindTaskFromProjectById(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull String projectId,
            @WebParam(name = "taskId", partName = "taskId") @NotNull String taskId
    );

    @Nullable
    @WebMethod
    List<Task> showAllTasksByProjectId(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull String projectId
    );

    @Nullable
    @WebMethod
    Task bindTaskToProjectById(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull String projectId,
            @WebParam(name = "taskId", partName = "taskId") @NotNull String taskId
    );
}
