package ru.vkandyba.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.model.Session;
import ru.vkandyba.tm.model.Task;
import ru.vkandyba.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IUserEndpoint {

    @Nullable
    @WebMethod
    User viewUserInfo(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    );

    @Nullable
    @WebMethod
    User changeUserPassword(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "password", partName = "password") @NotNull String password
    );


    @Nullable
    @WebMethod
    User updateUserProfile(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "firstName", partName = "firstName") @NotNull String firstName,
            @WebParam(name = "lastName", partName = "lastName") @NotNull String lastName,
            @WebParam(name = "midName", partName = "midName") @NotNull String midName
    );
}
