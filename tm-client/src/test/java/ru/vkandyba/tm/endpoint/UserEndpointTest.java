package ru.vkandyba.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vkandyba.tm.api.service.ServiceLocator;
import ru.vkandyba.tm.component.Bootstrap;
import ru.vkandyba.tm.marker.SoapCategory;

public class UserEndpointTest {

    @NotNull
    Session session = new Session();

    @NotNull
    ServiceLocator serviceLocator = new Bootstrap();

    @NotNull
    String login = "test";

    @NotNull
    String password = "test";

    @NotNull
    String firstName = "TestF";

    @NotNull
    String lastName = "TestL";

    @NotNull
    String midName = "TestM";


    @Before
    public void before(){
        session = serviceLocator.getSessionEndpoint().openSession(login, password);
    }

    @After
    public void after(){
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    @Category(SoapCategory.class)
    public void ChangePassword(){
        @Nullable final User user = serviceLocator.getUserEndpoint().changeUserPassword(session, password);
        Assert.assertNotNull(user);
    }

    @Test
    @Category(SoapCategory.class)
    public void ViewInfo(){
        @Nullable final User user = serviceLocator.getUserEndpoint().viewUserInfo(session);
        Assert.assertNotNull(user);
    }

    @Test
    @Category(SoapCategory.class)
    public void updateProfile(){
        @Nullable final User user = serviceLocator.getUserEndpoint().updateUserProfile(session, lastName, firstName, midName);
        Assert.assertNotNull(user);
    }
}
