package ru.vkandyba.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vkandyba.tm.api.service.ServiceLocator;
import ru.vkandyba.tm.component.Bootstrap;
import ru.vkandyba.tm.marker.SoapCategory;

import java.util.List;

public class TaskEndpointTest {

    @NotNull
    Session session = new Session();

    @NotNull
    ServiceLocator serviceLocator = new Bootstrap();

    @NotNull
    String login = "test";

    @NotNull
    String password = "test";

    @NotNull
    String TaskId = "123456790";

    @NotNull
    String TaskName = "test";

    @NotNull
    Integer TaskIndex = 0;

    @NotNull
    String userId = "1";

    @NotNull
    Status status = Status.COMPLETED;

    @Before
    public void before(){
        session = serviceLocator.getSessionEndpoint().openSession(login, password);
        final Task task = new Task();
        task.setId(TaskId);
        task.setUserId(userId);
        task.setName(TaskName);
        serviceLocator.getTaskEndpoint().addTask(session, task);
    }

    @After
    public void after(){
        serviceLocator.getTaskEndpoint().clearTasks(session);
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @Test
    @Category(SoapCategory.class)
    public void findAll() {
        List<Task> tasks = serviceLocator.getTaskEndpoint().findAllTasks(session);
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    @Category(SoapCategory.class)
    public void startById() {
        final Task task = serviceLocator.getTaskEndpoint().startTaskById(session, TaskId);
        Assert.assertNotNull(task);
    }

    @Test
    @Category(SoapCategory.class)
    public void startByIndex() {
        final Task task = serviceLocator.getTaskEndpoint().startTaskByIndex(session, TaskIndex);
        Assert.assertNotNull(task);
    }

    @Test
    @Category(SoapCategory.class)
    public void startByName() {
        final Task task = serviceLocator.getTaskEndpoint().startTaskByName(session, TaskName);
        Assert.assertNotNull(task);
    }

    @Test
    @Category(SoapCategory.class)
    public void finishById() {
        final Task task = serviceLocator.getTaskEndpoint().finishTaskById(session, TaskId);
        Assert.assertNotNull(task);
    }

    @Test
    @Category(SoapCategory.class)
    public void finishByIndex() {
        final Task task = serviceLocator.getTaskEndpoint().finishTaskByIndex(session, TaskIndex);
        Assert.assertNotNull(task);
    }

    @Test
    @Category(SoapCategory.class)
    public void finishByName() {
        final Task task = serviceLocator.getTaskEndpoint().finishTaskByName(session, TaskName);
        Assert.assertNotNull(task);
    }

    @Test
    @Category(SoapCategory.class)
    public void changeStatusById() {
        final Task task = serviceLocator.getTaskEndpoint().changeTaskStatusById(session, TaskId, status);
        Assert.assertNotNull(task);
    }

    @Test
    @Category(SoapCategory.class)
    public void changeStatusByIndex() {
        final Task task = serviceLocator.getTaskEndpoint().changeTaskStatusByIndex(session, TaskIndex, status);
        Assert.assertNotNull(task);
    }

    @Test
    @Category(SoapCategory.class)
    public void changeStatusByName() {
        final Task task = serviceLocator.getTaskEndpoint().changeTaskStatusByName(session, TaskName, status);
        Assert.assertNotNull(task);
    }

    @Test
    @Category(SoapCategory.class)
    public void removeById() {
        final Task task = serviceLocator.getTaskEndpoint().removeTaskById(session, TaskId);
        Assert.assertNotNull(task);
    }

    @Test
    @Category(SoapCategory.class)
    public void removeByIndex() {
        final Task task = serviceLocator.getTaskEndpoint().removeTaskByIndex(session, TaskIndex);
        Assert.assertNotNull(task);
    }

}
