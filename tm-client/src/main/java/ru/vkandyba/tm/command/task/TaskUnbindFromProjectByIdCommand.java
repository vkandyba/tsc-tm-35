package ru.vkandyba.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.endpoint.Session;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.util.TerminalUtil;

public class TaskUnbindFromProjectByIdCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "unbind-task-from-project-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Unbind task from project by id...";
    }

    @Override
    public void execute() {
        @NotNull final Session session = serviceLocator.getSession();
        System.out.println("Enter project id");
        @Nullable final String projectId = TerminalUtil.nextLine();
        System.out.println("Enter task id");
        @Nullable final String taskId = TerminalUtil.nextLine();
        serviceLocator.getTaskEndpoint().unbindTaskFromProjectById(session, projectId, taskId);
    }

    @NotNull
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
