package ru.vkandyba.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.endpoint.Project;
import ru.vkandyba.tm.endpoint.Session;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.exception.entity.ProjectNotFoundException;
import ru.vkandyba.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectStartByNameCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "project-start-by-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Start project by name...";
    }

    @Override
    public void execute() {
        @NotNull final Session session = serviceLocator.getSession();
        System.out.println("Enter name");
        @Nullable final String name = TerminalUtil.nextLine();
        @NotNull final Project project = serviceLocator.getProjectEndpoint().startProjectByName(session, name);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
