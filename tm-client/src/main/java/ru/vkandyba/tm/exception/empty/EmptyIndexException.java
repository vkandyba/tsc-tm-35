package ru.vkandyba.tm.exception.empty;

import ru.vkandyba.tm.exception.AbstractException;

public class EmptyIndexException extends AbstractException {

    public EmptyIndexException() {
        super("Error! Index is empty!");
    }

}
